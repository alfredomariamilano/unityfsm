﻿using UnityEditor;
using UnityEngine;
using UnityFSM.Editor;

namespace UnityFSM
{
    public abstract class LogicalStateMachine : StateMachineBehaviour
    {
        private const string Next = "Next";
        protected Animator ActiveAnimator;

        protected abstract void OnLogicalStateEnter(GameObject gameObject);

        protected abstract void OnLogicalStateUpdate(GameObject gameObject);

        protected abstract void OnLogicalStateExit(GameObject gameObject);

        protected abstract void ResetState(GameObject gameObject);

#if UNITY_EDITOR
        [InspectorButton("Continue", ButtonWidth = 150)]
        public bool ContinueButton;
#endif


        [MenuItem("UnityStateMachine/Continue")]
        protected void Continue()
        {
            if (!ActiveAnimator) return;
            ActiveAnimator.SetTrigger(Animator.StringToHash(Next));
        }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            ActiveAnimator = animator;
            ResetState(ActiveAnimator.gameObject);
            // by default all state machines will have a Next trigger
            ActiveAnimator.ResetTrigger(Animator.StringToHash(Next));

            OnLogicalStateEnter(ActiveAnimator.gameObject);
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            ActiveAnimator = animator;
            OnLogicalStateUpdate(ActiveAnimator.gameObject);
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            ActiveAnimator = animator;
            OnLogicalStateExit(ActiveAnimator.gameObject);
        }
    }
}