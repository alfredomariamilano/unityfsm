using UnityEngine;

namespace UnityFSM.Example
{
    public class ExampleState : LogicalStateMachine
    {
        protected override void OnLogicalStateEnter(GameObject gameObject)
        {
            Debug.Log("Entered!");
        }

        protected override void OnLogicalStateUpdate(GameObject gameObject)
        {
        }

        protected override void OnLogicalStateExit(GameObject gameObject)
        {
            Debug.Log("Exited!");
        }

        protected override void ResetState(GameObject gameObject)
        {
            Debug.Log("This is called before entering.");
        }
    }
}